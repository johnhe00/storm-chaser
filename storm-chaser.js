var FPS = 30;
var MAX_CATEGORY = 4;

var canvas;
var ctx;

var keys;
var player;
var storms;
var category;
var score;

var COLORS = {
	player: "#00f",
	storms: ["#ccc", "#999", "#666", "#333", "#000"]
};

var KEYBIND = {
	up: 87,
	left: 65,
	down: 83,
	right: 68
}

window.onload = init;

function init() {
	canvas = document.getElementById("canvas");
	canvas.height = 600;
	canvas.width = 600;
	ctx = canvas.getContext("2d");
	score = document.getElementById("score");
	initBoard();
	start();
}

function start() {
	addStorm();
	
	addEventListener("keydown", keyDown);
	addEventListener("keyup", keyUp);
	setInterval(update, 1000/FPS);
	setInterval(updateScore, 1000);
	setInterval(addStorm, 15000);
}

function initBoard() {
	storms = [];
	category = 0;
	player = {
		x: canvas.width / 2,
		y: canvas.height / 2,
		radius: 10,
		velX: 6,
		velY: 6
	};
	keys = {
		up: false,
		left: false,
		down: false,
		right: false
	};
}

function addStorm() {
	var storm = {
		x:0,
		y:0,
		radius: player.radius + Math.floor(category),
		velX: Math.floor(category) + 1,
		velY: Math.floor(category) + 1
	};

	if(canvas.width - player.x < player.x) {
		storm.x = storm.radius;
	}
	else {
		storm.x = canvas.width - storm.radius;
	}

	if(canvas.height - player.y < player.y) {
		storm.y = storm.radius;
	}
	else {
		storm.y = canvas.height - storm.radius;
	}

	if(category != MAX_CATEGORY) {
		category += 0.5;
	}
	storms.push(storm);
}

function draw() {
	ctx.clearRect(0,0,canvas.width, canvas.height);
	
	//Draw the player//
	drawCircle(player.x, player.y, player.radius, COLORS.player);
	
	//Draw the Storms//
	for(var i = 0; i < storms.length; i++) {
		var storm = storms[i];
		drawCircle(storm.x, storm.y, storm.radius, COLORS.storms[storm.radius - player.radius]);
	}
}

function drawCircle(x, y, r, c) {
	ctx.beginPath();
	ctx.arc(x, y, r, 0, 2*Math.PI);
	ctx.fillStyle = c;
	ctx.fill();
	ctx.stroke();
}

function keyDown(e) {
	switch(e.which) {
	case KEYBIND.up:
		keys.up = true;
		break;
	case KEYBIND.left:
		keys.left = true;
		break;
	case KEYBIND.down:
		keys.down = true;
		break;
	case KEYBIND.right:
		keys.right = true;
	default:
		break;
	}
}

function keyUp(e) {
	switch(e.which) {
	case KEYBIND.up:
		keys.up = false;
		break;
	case KEYBIND.left:
		keys.left = false;
		break;
	case KEYBIND.down:
		keys.down = false;
		break;
	case KEYBIND.right:
		keys.right = false;
	default:
		break;
	}
}

function update() {
	if (keys.up) {
		if(player.y - player.radius >= player.velY) {
                	player.y -= player.velY;
		}
		else {
			player.y = player.radius;
		}
        }
        if(keys.left) {
		if(player.x - player.radius >= player.velX) {
                	player.x -= player.velX;
		}
		else {
			player.x = player.radius;
		}
        }
        if(keys.down) {
		if(player.y + player.radius <= canvas.height - player.velY) {
	                player.y += player.velY;
		}
		else {
			player.y = canvas.height - player.radius;
		}
        }
        if(keys.right) {
		if(player.x + player.radius <= canvas.width - player.velX) {
	                player.x += player.velX;
		}
		else {
			player.x = canvas.width - player.radius;
		}
        }
	
	for(var i = 0; i < storms.length; i++) {
		moveStorm(storms[i]);
	}

	draw();
}

function updateScore() {
	score.innerHTML++;
}

function moveStorm(storm) {
	if(storm.x < player.x) {
		storm.x += storm.velX;
	}
	else if(storm.x > player.x) {
		storm.x -= storm.velX;
	}
	
	if(storm.y < player.y) {
		storm.y += storm.velY;
	}
	else if(storm.y > player.y) {
		storm.y -= storm.velY;
	}
}
